var base64 = require('base-64');
var utf8 = require('utf8');

//ENCODING DATA
const encoding = function(text){
    var bytes = utf8.encode(text);
    var encoded = base64.encode(bytes);
    console.log(encoded);
}


//DECODING DATA
const decoding = function(encoded){
    var bytes = base64.decode(encoded);
    var text = utf8.decode(bytes);
    console.log(text);
}

encoding('HiHelloOkay');
decoding('SGlIZWxsb09rYXk=');